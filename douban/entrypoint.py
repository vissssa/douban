#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 8:58
# @Author  : Zhangyu
# @Site    : 
# @File    : entrypoint.py
# @Software: PyCharm
from scrapy.cmdline import execute

# 豆瓣最新电影
execute(['scrapy', 'crawl', 'douban'])
# 豆瓣Top250
# execute(['scrapy', 'crawl', 'top250'])
