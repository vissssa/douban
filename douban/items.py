# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MovieItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    cover = scrapy.Field()
    rate = scrapy.Field()
    url = scrapy.Field()
    id = scrapy.Field()
    is_new = scrapy.Field()
    updatetime = scrapy.Field()
    releasedate = scrapy.Field()


class Top250Item(scrapy.Item):
    id = scrapy.Field()
    rank = scrapy.Field()
    title = scrapy.Field()
    cover = scrapy.Field()
    rate = scrapy.Field()
    url = scrapy.Field()
    remark = scrapy.Field()
    updatetime = scrapy.Field()
