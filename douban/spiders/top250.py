#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-26 13:13
# @Author  : Zhangyu
# @Site    : 
# @File    : top250.py
# @Software: PyCharm
import scrapy
from scrapy.http import Request
from datetime import datetime
from douban.items import Top250Item
from douban.mysqlpipelines.sql import Sql


class TopSpider(scrapy.Spider):
    name = 'top250'
    allowed_domains = ['douban.com']
    start_urls = ['https://movie.douban.com/top250']

    def parse(self, response):
        items = Top250Item()
        moviesList = response.xpath('//*[@id="content"]/div/div[1]/ol/li')
        for movies in moviesList:
            items['url'] = movies.xpath('div/div[1]/a/@href').extract_first()
            items['rank'] = movies.xpath('div/div[1]/em/text()').extract_first()
            items['id'] = str(items['url']).split('/')[-2]
            items['cover'] = movies.xpath('div/div[1]/a/img/@src').extract_first()
            items['title'] = movies.xpath('div/div[1]/a/img/@alt').extract_first()
            items['rate'] = movies.xpath('div/div[2]/div[2]/div/span[2]/text()').extract_first()
            items['remark'] = movies.xpath('div/div[2]/div[2]/p[2]/span/text()').extract_first()
            items['updatetime'] = str(datetime.now())
            yield items
        next_page = response.xpath('//*[@id="content"]/div/div[1]/div[2]/span[3]/a/@href').extract_first()
        if next_page:
            next_url = response.urljoin(next_page)
            yield Request(next_url, callback=self.parse)
        else:
            Sql.close()
