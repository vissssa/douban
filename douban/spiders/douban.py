#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-23 17:48
# @Author  : Zhangyu
# @Site    :
# @File    : logindb.py
# @Software: PyCharm
import scrapy
import urllib.request
# import requests
from scrapy.http import Request
import urllib
from PIL import Image
from douban.items import MovieItem
import json
from datetime import datetime
from douban.mysqlpipelines.sql import Sql

meta = {
    'dont_redirect': True,  # 禁止网页重定向
    'handle_httpstatus_list': [301, 302]  # 对哪些异常返回进行处理
}


class LogindbSpider(scrapy.Spider):
    name = 'douban'
    allowed_domains = ['douban.com']

    #    start_urls = ['http://www.douban.com/']

    # def start_requests(self):
    #     '''
    #     重写start_requests，请求登录页面
    #     '''
    #     # res = requests.get('https://accounts.douban.com/login')
    #     # print(res.status_code)
    #     # print(res.text)
    #     try:
    #         Sql.truncate_doubanMovie()
    #     except Exception as e:
    #         print(e)
    #         pass
    #     else:
    #         print("截断表")
    #     yield Request("https://accounts.douban.com/login", callback=self.parse_before_login)
    #
    # def parse_before_login(self, response):
    #     '''
    #     登录表单填充，查看验证码
    #     '''
    #     print("登录前表单填充")
    #     captcha_id = response.xpath('//input[@name="captcha-id"]/@value').extract_first()
    #     captcha_image_url = response.xpath('//img[@id="captcha_image"]/@src').extract_first()
    #     if captcha_image_url is None:
    #         print("登录时无验证码")
    #         formdata = {
    #             'ck': 'PJFN',
    #             "source": "None",
    #             "redir": "https://www.douban.com",
    #             "form_email": "18056004315",
    #             "form_password": "zhangyu292724",
    #             "login": "登录",
    #         }
    #     else:
    #         print("登录时有验证码")
    #         save_image_path = "/root/captcha.jpeg"
    #         # 将图片验证码下载到本地
    #         urllib.request.urlretrieve(captcha_image_url, save_image_path)
    #         # 打开图片，以便我们识别图中验证码
    #         try:
    #             im = Image.open('captcha.jpeg')
    #             im.show()
    #         except:
    #             pass
    #         # print('根据打开的图片输入验证码:')
    #         # 手动输入验证码
    #         captcha_solution = input('根据打开的图片输入验证码:')
    #         formdata = {
    #             'ck': 'PJFN',
    #             "source": "None",
    #             "redir": "https://www.douban.com",
    #             "form_email": "18056004315",
    #             # 此处请填写密码
    #             "form_password": "zhangyu292724",
    #             "captcha-solution": captcha_solution,
    #             "captcha-id": captcha_id,
    #             "login": "登录",
    #         }
    #
    #     print("登录中")
    #     # 提交表单
    #     return scrapy.FormRequest.from_response(response, formdata=formdata,
    #                                             callback=self.parse_after_login)
    #
    # def parse_after_login(self, response):
    #     '''
    #     验证登录是否成功
    #     '''
    #     # print(response.body)
    #     account = response.xpath('//a[@class="bn-more"]/span/text()').extract_first()
    #     if account is None:
    #         print("登录失败")
    #     else:
    #         print(u"登录成功,当前账户为 %s" % account)
    #     urllist = response.xpath('//*[@id="db-global-nav"]/div/div[4]//a/@href').extract()
    #     for url in urllist:
    #         if 'movie.douban.com' in url:
    #             print(url)
    #             yield Request(url, callback=self.get_movie_page)

    def start_requests(self):
        # moreInfo_url = str(response.url) + response.xpath(
        #     '//*[@id="content"]/div/div[2]/div[4]/div[2]/h2/a/@href').extract_first()

        # 直接打开选电影页面不能加载电影信息，这个是电影列表json格式
        '''
        注意，一定要小心重定向问题
        :param response:
        :return:
        '''

        try:
            Sql.truncate_doubanMovie()
        except Exception as e:
            print(e)
            pass
        else:
            print("截断表")
        for num in range(0, 320 + 1):
            if num % 20 == 0:
                moreInfo_url = 'https://movie.douban.com/j/search_subjects?type=movie&tag=%E7%83%AD%E9%97%A8&' \
                               'sort=recommend&page_limit=20&page_start={}'.format(num)
                yield Request(moreInfo_url, callback=self.getHotMovies, meta=meta)

    def getHotMovies(self, response):
        print('开始获取电影信息')
        data = json.loads(response.text)
        dict_meta = {}
        for infoDict in data['subjects']:
            dict_meta['rate'] = infoDict['rate']
            dict_meta['title'] = infoDict['title']
            dict_meta['url'] = infoDict['url']
            dict_meta['id'] = infoDict['id']
            dict_meta['cover'] = infoDict['cover']
            dict_meta['is_new'] = infoDict['is_new']
            dict_meta['updatetime'] = str(datetime.now())
            yield Request(dict_meta['url'], meta=dict_meta, callback=self.getReleaseDate)

    def getReleaseDate(self, response):
        items = MovieItem()
        infoDict = response.meta
        items['rate'] = infoDict['rate']
        items['title'] = infoDict['title']
        items['url'] = infoDict['url']
        items['id'] = infoDict['id']
        items['cover'] = infoDict['cover']
        items['is_new'] = infoDict['is_new']
        items['updatetime'] = str(datetime.now())
        list_releasedate = response.xpath('//*[@property="v:initialReleaseDate"]/text()').extract()
        items['releasedate'] = '/'.join(list_releasedate)
        yield items
