#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 10:12
# @Author  : Zhangyu
# @Site    : 
# @File    : pipelines.py
# @Software: PyCharm
from .sql import Sql
from douban.items import MovieItem, Top250Item


class MoviePipeline(object):

    def process_item(self, items, spaider):
        if isinstance(items, MovieItem):
            ret = Sql.select_doubanMovie_id(items['id'])
            if ret == 1:
                print(u"已经存在了")
                pass
            else:
                Sql.insert_doubanMovie(items['id'], items['title'], items['rate'], items['cover'], items['url'],
                                       items['is_new'], items['updatetime'], items['releasedate'])
                print("complete {}".format(items['title']))
        if isinstance(items, Top250Item):
            try:
                Sql.delete_doubanTop250()
                Sql.insert_doubanTop250(items['id'], items['title'], items['rate'], items['cover'], items['url'],
                                        items['rank'], items['updatetime'], items['remark'])
                print("complete {}".format(items['title']))
            except:
                Sql.insert_doubanTop250(items['id'], items['title'], items['rate'], items['cover'], items['url'],
                                        items['rank'], items['updatetime'], items['remark'])
                print("complete {}".format(items['title']))
