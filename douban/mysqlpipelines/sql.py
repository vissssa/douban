#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 10:12
# @Author  : Zhangyu
# @Site    : 
# @File    : sql.py
# @Software: PyCharm
import MySQLdb
from douban import settings

mysql_con = MySQLdb.connect(settings.HOSTS, settings.USERS, settings.PASSWORD, settings.DB, charset='utf8')
cursor = mysql_con.cursor()


class Sql:

    @classmethod
    def truncate_doubanMovie(cls):
        sql = "truncate table doubanMovie"
        cursor.execute(sql)
        mysql_con.commit()

    @classmethod
    def insert_doubanMovie(cls, id, title, rate, cover, url, is_new, updatetime, releasedate):
        sql = 'INSERT INTO doubanMovie (id, title, rate, cover, url, is_new, updatetime, releasedate) VALUES ' \
              '(%(id)s,%(title)s,%(rate)s,%(cover)s,%(url)s,%(is_new)s,%(updatetime)s, %(releasedate)s)'
        value = {
            'id': int(id),
            'title': title,
            'rate': rate,
            'cover': cover,
            'url': url,
            'is_new': is_new,
            'updatetime': updatetime,
            'releasedate': releasedate
        }
        cursor.execute(sql, value)
        mysql_con.commit()

    @classmethod
    def select_doubanMovie_id(cls, id):
        sql = 'SELECT EXISTS(SELECT 1 FROM doubanMovie WHERE id=%(id)s)'
        value = {
            'id': id
        }
        cursor.execute(sql, value)
        return cursor.fetchall()[0][0]

    @classmethod
    def insert_doubanTop250(cls, id, title, rate, cover, url, rank, updatetime, remark):
        sql = 'INSERT INTO doubanTop250 (id, title, rate, cover, url, rank, updatetime, remark) VALUES ' \
              '(%(id)s,%(title)s,%(rate)s,%(cover)s,%(url)s,%(rank)s,%(updatetime)s,%(remark)s)'
        value = {
            'id': int(id),
            'title': title,
            'rate': rate,
            'cover': cover,
            'url': url,
            'rank': int(rank),
            'updatetime': updatetime,
            'remark': remark,
        }
        cursor.execute(sql, value)
        mysql_con.commit()

    @classmethod
    def delete_doubanTop250(cls):
        sql1 = "SELECT MAX(rank) from doubanTop250"
        cursor.execute(sql1)
        if cursor.fetchall()[0][0] >= 250:
            sql = 'truncate table doubanTop250'
            cursor.execute(sql)
            mysql_con.commit()

    @classmethod
    def close(cls):
        cursor.close()
        mysql_con.close()
